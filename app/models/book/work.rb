class Book::Work
  include Mongoid::Document
  include Mongoid::Timestamps

  embedded_in :category, class_name: '::Book::Work::Category', inverse_of: :book_works
  embedded_in :package, class_name: '::Book::Package', inverse_of: :works

  field :name_full,      type: String

end