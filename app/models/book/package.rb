class Book::Package
  include Mongoid::Document
  include Mongoid::Timestamps

  field :name_short,        type: String
  field :name_full,         type: String
  field :name_abbr,         type: String
  field :name_market,       type: String
  field :instruction,       type: String
  field :cost,              type: Float
  # field :aworks, type: Array

  embeds_many :works, class_name: '::Book::Work', inverse_of: :package
  # embeds_many :illustrations, class_name: '::Book::Package::Illustration', inverse_of: :book_package
  # embeds_many :teeth_pictures, class_name: '::Book::Package::Picture', inverse_of: :book_package


end