class Book::Work::Category
  include Mongoid::Document
  include Mongoid::Timestamps
  # include Mongoid::Tree

  before_destroy :destroy_children

  field :name, type: String

  belongs_to :user, class_name: '::User'
  embeds_many :book_works, class_name: '::Book::Work', inverse_of: :categories
end