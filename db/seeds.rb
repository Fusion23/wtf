# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

user = {
    email: 'user@example.com',
    password: '12345678',
}

# user = User.find_by(email: user_options[:email])
# user = User.create(user) if user.nil?
if User.where(email: user[:email]).count == 0
  user = User.create(user)
else
  user = User.where(email: user[:email]).first
end